extends Node

#
# Loading Manager (g_loading_manager)
# def: Allows to load a resource dynamicly
#

# Signal raised when loading is done
# @arg the resource loaded
signal loading_finished

# The scene displayed while loading
var loading_scene = preload("res://scenes/scn_loading/scn_loading.tscn")
var loading_instance = null

# Loader - Godot class while loading dynamicly
var loader = null

# The resulting resource
var resource_loaded = null

# Time spent loading
var loading_duration = 0

# Ready 
func _ready():
	connect("loading_finished", self, "_on_loading_finished")

# Return true if is loading
func is_loading():
	return is_processing() == true

# Process
func _process(delta):
	loading_duration += delta
	
	# Timeout management
	if loading_duration >= Globals.loading_time_duration:
		print("g_loading_manager._process(): Loading timed out !")
		yield()
	
	# Are we loading ?
	if loader :
	    # Poll the loader
		var status = loader.poll()
		var progress_bar = loading_instance.progress_bar
	
		# Status update
		if status == ERR_FILE_EOF: # load finished
			progress_bar.set_max( loader.get_stage_count() )
			progress_bar.set_val( loader.get_stage() )
			resource_loaded = loader.get_resource()
			unload_loading_instance()
			loader = null
			set_process(false)
			emit_signal("loading_finished", resource_loaded)
			print("g_loading_manager._process(): Interactive load succeeded !")
		
		# Processing
		elif status == OK:
			progress_bar.set_max( loader.get_stage_count() )
			progress_bar.set_val( loader.get_stage() )
		
		# Error
		else: 
			loader = null
			unload_loading_instance()
			print("g_loading_manager._process(): ERROR: Interactive load failed with status ", status , ".")
			yield() # exit

# Use this to load interactivly a resource
func load_interactive( p_path ):
	if loader != null:
		print("g_loading_manager.load_interactive() : Cannot load two resource at the same time !")
		yield()
	else:
		print("g_loading_manager.load_interactive() : Started loading of resource at " + p_path +".")
	
	loader = ResourceLoader.load_interactive(p_path)
	loading_instance = loading_scene.instance() 
	get_node("/root").add_child( loading_instance )
	
	
	set_process(true)
	loading_duration = 0

# Free the loading scene instance
func unload_loading_instance():
	if get_node("/root").has_node(loading_instance.get_name()):
		get_node("/root").remove_child( loading_instance )

# Callback on loading finished
func _on_loading_finished(p_loaded_resource):
	unload_loading_instance()