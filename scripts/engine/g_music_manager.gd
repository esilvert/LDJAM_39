extends Node

#
# Music Manager (g_music_manager)
# def: Allows to manipulate music
#

# TODO: interpolation between two musics so the volume is progressive 

# Map of music
var musics = {}

# The current music being played
var current_music = null
var current_music_path = null

# Play a music
func play_music(p_path, p_loop = true, p_keep_current_progress = false):
	print("g_music_manager.play_music(): Asked to play : " , p_path )
	
	# Exit condition
	if current_music != null and current_music["path"] == p_path :
		return
	
	var is_new_music = musics.keys().has(p_path) == false
	# Prepare new stream player
	if is_new_music == true:
		musics[p_path] = {"stream_player" : StreamPlayer.new(), "time": 0, "path" : p_path}
		add_child(musics[p_path]["stream_player"])
	
	# Keep progress or stop
	if current_music != null:
		var old_path = current_music["path"]
		current_music["stream_player"].stop()
		if p_keep_current_progress == true:
			musics[old_path]["time"] = musics[old_path]["stream_player"].get_pos()
		else:
			musics[old_path]["time"] = 0
			
	# Configure music
	current_music = musics[p_path]
	var stream_player = musics[p_path]["stream_player"]
	musics[p_path]["path"] = p_path
	print (musics[p_path])
	var music_stream = ResourceLoader.load(Globals.music_folder_path + p_path + ".ogg")
	stream_player.set_stream( music_stream )
	stream_player.set_autoplay(true)
	stream_player.set_loop(p_loop)
	
	stream_player.play(musics[p_path]["time"] )

func stop_music():
	current_music["stream_player"].stop()
	current_music_path = ""
	