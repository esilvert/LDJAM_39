extends Node

#
# Sound Manager (g_sound_manager)
# def: Allows to play a sound
#

# The sample player
onready var sample_player = SamplePlayer.new()

var sample_library = load("res://assets/sample_library/global.tres")

# Ready
func _ready():
	sample_player.set_sample_library( sample_library )

# Play (and possibly load) a sound
func play(p_id, p_uniq = false):
	if not sample_library.has_sample(p_id) :
		var sample = ResourceLoader.load(Globals.sound_folder_path + p_id + ".wav")
		sample_library.add_sample( p_id, sample )
	sample_player.play(p_id, p_uniq)