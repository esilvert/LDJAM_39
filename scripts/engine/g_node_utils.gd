extends Node

#
# NodeUtils (g_node_utils)
# def: Safe operation on nodes
#

# Secure get_node (can't return null)
func get_node(p_origin, p_path):
	g_debug.assert_message(p_origin != null, "g_node_utils.get_node(): Origin is null.")
	
	if p_origin.has_node(p_path):
		return p_origin.get_node(p_path)
	else:
		g_debug.print_message("g_node_utils.get_node(): No child node " + p_path + " in " + p_origin.get_name() +".")
		assert(false)
		
# Secure get_node (can return null)
func try_get_node(p_origin, p_path):
	g_debug.assert_message(p_origin != null, "g_node_utils.get_node(): Origin is null.")
	return p_origin.get_node(p_path)

# get sibbling
func get_sibbling(p_origin, p_path):
	return self.get_node(p_origin, "../" + p_path)

# recursively set_visible
func recursive_set_visible(p_origin, p_value):
	g_debug.assert_message(p_origin != null, "g_node_utils.recursive_set_visible(): Origin is null.")
	
	if p_origin.has_method("set_hidden"):
		p_origin.set_hidden( not p_value )
		
		for child in p_origin.get_children():
			recursive_set_visible(child, p_value)
