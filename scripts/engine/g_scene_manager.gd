extends Node
#
# Scene manager (g_scene_manager)
# def: Allow to work on scenes
#

# Signal when scene has changed
signal scene_changed

# Current scene played, synchronized with get_tree().current_scene
var current_scene = null
var current_scene_packed = null
var _next_scene = null # intern use

# The color used during fading
var fading_color = Color(0,0,0)
	
# Switch to packed scene
func go_to_scene_packed( p_packed_scene):
	call_deferred("_go_to_scene_packed", p_packed_scene)
	g_render_manager.fade_to(fading_color)
	
# Effective go_to scene packed
func _go_to_scene_packed( p_packed_scene ):
	current_scene_packed = p_packed_scene
	_switch_scene_fading( p_packed_scene.instance() )

# Go to a scene given by its path
func go_to_scene_path( p_path) :
	g_loading_manager.connect("loading_finished", self, "_on_resource_loaded")
	g_loading_manager.load_interactive(Globals.scene_folder_path + p_path + "/" + p_path + ".tscn")
	g_render_manager.fade_to(fading_color)

# Callback while loading a scene
func _on_resource_loaded(p_new_scene):
	current_scene_packed = p_new_scene
	_switch_scene_fading(p_new_scene.instance())
	g_loading_manager.disconnect("loading_finished", self, "_on_resource_loaded")

# Switch with fading
func _switch_scene_fading(p_new_scene):
	_next_scene = p_new_scene
	set_process(true)

# Configure new scene
func _switch_scene( p_new_scene ):
	if current_scene != null:
		get_tree().get_root().remove_child(current_scene)
		current_scene.queue_free()
	else:
		get_tree().get_current_scene().free()
	
	current_scene = p_new_scene
	get_node("/root").add_child(p_new_scene)
	get_tree().set_current_scene(p_new_scene)
	emit_signal("scene_changed", p_new_scene)
	print( "g_scene_manager._switch_scene(): Switched scene.")

# Process
func _process(delta):
	if _next_scene != null and g_render_manager.is_fading == false:
		_switch_scene(_next_scene)
		g_render_manager.fade_to( Color(1,1,1) )
		_next_scene = null

# Reload the current scene
func reload_current_scene():
	g_sound_manager.play("death")
	if current_scene_packed != null:
		_switch_scene(current_scene_packed.instance())
	else:
		get_tree().reload_current_scene()
	