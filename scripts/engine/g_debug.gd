extends Node

#
# Debug (g_debug)
# def: Allow to debug
#

# Start time 
onready var start_time = OS.get_ticks_msec()

# Print a message
func print_message(p_message):
	print( "[%10.3f" % (OS.get_ticks_msec() * 0.001 - start_time * 0.001), "]\t",  p_message )

# Print a message on assert
func assert_message(p_condition, p_message):
	if p_condition == false:
		print_message("Assertion failed : " + p_message)
		assert(p_condition)
		