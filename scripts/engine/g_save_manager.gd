extends Node

#
# Save Manager (g_save_manager)
# def: Allows to keep data in a save file
#


# The JSON saved calling the save() method
var save_datas = {  
	"scene" : null 
}

# Launch save process
# WARNING: Don't save until you already have used g_scene_manager._switch_scene() once.
func save(p_file_name = Globals.default_save_file_name) : 
	# Open file
	var save_file = File.new()
	save_file.open(get_save_path(p_file_name), File.WRITE)
	
	# First save current 
	save_datas["scene"] = {"filename" : get_tree().get_current_scene().get_filename()}
	save_file.store_line( save_datas.to_json() )
	
	# Get the nodes with persistent data
	var savenodes = get_tree().get_nodes_in_group(Globals.save_node_group_name)
	
	# Call save for each node
	for node in savenodes :
		print("g_save_manager.save(): Saving node : " , node, "..." )
		assert( node.has_method("save") )
		node.save(save_file)
	
	# End save process
	save_file.close()
	print("g_save_manager.save(): Game successfully saved.")

# Quit after saving
func save_and_exit():
	save()
	get_tree().quit()

# Load a save file and call @p_load_method on the resulting JSON
func get_json_from_save(p_node, p_method_name, p_filename = Globals.get("default_save_file_name")):
	# Open the save file
	var save_file = File.new()
	save_file.open(get_save_path(p_filename), File.READ)
	
	# Load datas
	var lines = {}
	lines.parse_json( save_file.get_line() )
	
	# Prepared current scene
	var scene = load(lines["scene"]["filename"]).instance()
	g_scene_manager.go_to_scene(scene, false) # do not call_deferred
	
	# Delete all save dependent nodes
	var nodes_to_delete = get_tree().get_nodes_in_group(Globals.delete_on_load_group_name)
	for node in nodes_to_delete :
		node.queue_free()
	
	# Now call the method with lines
	p_node.call(p_method_name, lines)
	
	# Clear process
	save_file.close()
	
func get_save_path(p_save_name = Globals.get("default_save_file_name")):
	return "user://" + p_save_name +".save"