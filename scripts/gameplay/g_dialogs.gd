extends Node2D

var dialogs = {
	"intro_1" : {
		"texture" : "unknown",
		"text" : "Welcome to your new reality dumb ass.",
		"next" : "intro_2"
	},
	
	"intro_2" : {
		"texture" : "squeleton",
		"text" : "Who's.. Who's talking ?",
		"next" : "intro_3"
	},
	
	"intro_3" : {
		"texture" : "unknown",
		"text" : "Open your eyes! Ooooh, sorry, you don't have eyes anymore. Hahaha",
		"next" : "intro_4"
	},
	
	"intro_4" : {
		"texture" : "squeleton",
		"text" : "That's not nice. Did I just die ?",
		"next" : "intro_5"
	},
	
	"intro_5" : {
		"texture" : "unknown",
		"text" : "Are you f*cking serious man ?",
		"next" : "intro_6"
	},
	
	"intro_6" : {
		"texture" : "imp",
		"anim" : "imp_enter",
		"text" : "Look at you man, you're a set of bone; you're just living to distract our Lord's dog, Cerberus.",
		"next" : "intro_7"
	},
	
	"intro_7" : {
		"texture" : "squeleton",
		"text" : "Please, I don't want to die.. I.. I am scared; help me getting out please !",
		"next" : "intro_8"
	},
	
	"intro_8" : {
		"texture" : "imp",
		"text" : "Will you help me back once out ?",
		"next" : "intro_9"
	},
	
	"intro_9" : {
		"texture" : "squeleton",
		"text" : "I will do whatever you want ! Please ! ",
		"next" : "intro_10"
	},
	
	"intro_10" : {
		"texture" : "imp",
		"text" : "Let the fun begin then, I will teach you using your new body.",
	},
	
	"tuto_jump_1_1": {
		"texture" : "imp",
		"anim" : "tuto_jump",
		"text" : "Hey dead boy. Press f*cking Z/W or Up to jump.",
		"next" : "tuto_jump_1_2"
	},
	
	"tuto_jump_1_2": {
		"texture" : "squeleton",
		"text" : "You are very rude sir Devil.",
		"next" : "tuto_jump_1_3"
	},
	
	"tuto_jump_1_3": {
		"texture" : "imp",
		"text" : "Hell no ! I'm not The Lord idiot; just jump over this sh*t. ",
		"next" : "tuto_jump_1_4"
	},
	
	"tuto_jump_1_4": {
		"texture" : "imp",
		"text" : "(He's in the freaking Hell and he focuses on my language ? Idiot) ",
	},
	
	"tuto_jump_2_1": {
		"texture" : "imp",
		"text" : "Congrats, you just jumped without any muscle; proud of ya ?",
		"next" : "tuto_jump_2_2"
	},
	
	"tuto_jump_2_2": {
		"texture" : "squeleton",
		"text" : "I feel so light, but I barely understand my condition right now... Will I recover my body ?",
		"next" : "tuto_jump_2_3"
	},
	
	"tuto_jump_2_3": {
		"texture" : "imp",
		"text" : "Let's hope you recover a brain at least ! Now jump highter keeping pressing the jump key. This one is a bit taller so you can train.",
	},
	
	"tuto_throw": {
		"texture" : "imp",
		"text" : "*Bored* Now let's use your body in a more... efficient way.",
		"next" : "tuto_throw_2"
	},
	
	"tuto_throw_2": {
		"texture" : "imp",
		"text" : "Put your mouse on this target. Hold the left button to focus on it. The longer you wait, the further you will throw one of your body member.",
		"next" : "tuto_throw_3"
	},
	
	"tuto_throw_3": {
		"texture" : "squeleton",
		"text" : "Wait, WHAT ?! Am I really going to throw my arm on this target ? Why would I do so anyway ?",
		"next" : "tuto_throw_4"
	},
	
	"tuto_throw_4": {
		"texture" : "imp",
		"text" : "You ain't a genius, ain you ? If you attach your arm on this wall, you will be able to jump on it and go through.",
		"next" : "tuto_throw_5"
	},
	
	"tuto_throw_5": {
		"texture" : "squeleton",
		"anim" : "throw_target_appear",
		"text" : "Well, okay then... What if I miss the target ? I'm not.. used to .. Throw my very arm; kind of my first time...",
		"next" : "tuto_throw_6"
	},
	
	"tuto_throw_6": {
		"texture" : "imp",
		"text" : "Oh jes.. f*ck ! I mean go.. SH*T ! Anyway, if you miss, go close to it and reput it on you, you idiot. Press E, Right Click or Enter near the arm to get it back.",
	},
	
	"joke_arm": {
		"texture" : "imp",
		"text" : "*giggling* Hey... hey... That last training... Costed you an arm ! *Laughing out loud* Oh man, this is gonna be so FUN !",
	},
	
	"joke_arm": {
		"texture" : "imp",
		"text" : "*giggling* Hey... hey... That last training... Costed you an arm ! *Laughing out loud* Oh man, this is gonna be so FUN !",
		"next": "joke_arm_2"
	},
	
	"joke_arm_2": {
		"texture" : "imp",
		"text" : "Keep on going, don't miss this one. The last one had rotten flesh, his arms didn't stick to the wall and he's never seen it afterward.",
		"next": "joke_arm_3"
	},
	
	"joke_arm_3": {
		"texture" : "squeleton",
		"text" : "(I don't feel safe with this freak, I should not miss this one.)",
	},
	
	"leg_shot": {
		"texture" : "imp",
		"text" : "Oh, you suceeded. Well, you still have you legs right ? Gargoyles usually guard the portals. You will need to distract them with a bone to open the doors. Find them, shoot them and you'll will pass.",
	},
	
	"lvl_2_intro": {
		"texture" : "squeleton",
		"text" : "Where are we going exactly ?",
		"next" : "lvl_2_intro_2"
	},
	
	"lvl_2_intro_2": {
		"texture" : "imp",
		"text" : "That's none of your business bog of bones. Go ahead and stop talking, you have such a terrible voice.",
	},
	
	"lvl_3_intro": {
		"texture" : "unknown",
		"text" : "(A loud voice is talking) I feel a strange presence here, come to me, feed my hunger lost soul.",
		"next" : "lvl_3_intro_2"
	},
	
	"lvl_3_intro_2": {
		"texture" : "imp",
		"text" : "Oops ! It appears the Lord felt your dumb lost soul man, you're in a deep sh*t! Hahaha !",
		"next" : "lvl_3_intro_3"
	},
	
	"lvl_3_intro_3": {
		"texture" : "squeleton",
		"text" : "Was that... the Devil ..? What does he want from me ? I did nothing wrong ! Why am I here in the first place ?!",
		"next" : "lvl_3_intro_4"
	},
	
	"lvl_3_intro_4": {
		"texture" : "imp",
		"text" : "Keep talking little boy, the Lord is coming and I can't wait to see what he's prepared for you !",
		"next" : "lvl_3_intro_5"
	},
	
	"lvl_3_intro_5": {
		"texture" : "squeleton",
		"text" : "Please, just tell me where to go !",
		"next" : "lvl_3_intro_6"
	},
	
	"lvl_3_intro_6": {
		"texture" : "imp",
		"text" : "Well, either left or right, you won't be able to try both. Left is the platform training area, lot of jumps incoming, deaths pits, terrible efforts, the hardest route to the exit. Only TRUE challengers try it...",
		"next" : "lvl_3_intro_7"
	},
	
	"lvl_3_intro_7": {
		"texture" : "imp",
		"text" : "If you go right, you will find the shoot training area, you'll have to aim, maybe you will have to retry but accuracy is the key. No jump, one single shot. Just try and if you're blocked, hit the retry button on the up left corner and pray that I teleport you at the very start of the area. Anyway, you must know that here the lava is quite solid, you can walk on it so go ahead.",
	},
	
	"lvl_3_joke": {
		"texture" : "unknown",
		"text" : "Hahaha yes, come closer little boy, let's have some fun together. In this place, nothing is forbidden.",
		"next" : "lvl_3_joke_2"
	},
	
	"lvl_3_joke_2": {
		"texture" : "squeleton",
		"text" : "Closer ? What does he mean ? Are you sure we're on the right way ? The way home ?",
		"next" : "lvl_3_joke_3"
	},
	
	"lvl_3_joke_3": {
		"texture" : "imp",
		"text" : "Of course ! You swore to help me if I help you, we are [i]friend[/i]. No go ahead and don't bother, the Lord hardly moves from his Ironic Throne.",
	},
	
	"lvl_3_aim": {
		"texture" : "imp",
		"text" : "Aaaaand, this is the shoting area, loser !",
		"next" : "lvl_3_aim_2"
	},
	
	"lvl_3_aim_2": {
		"texture" : "squeleton",
		"text" : "Eeeeh... Well, you might be excited but it won't be as easy as I though right; you're always fooling me !",
		"next" : "lvl_3_aim_3"
	},
	
	"lvl_3_aim_3": {
		"texture" : "imp",
		"text" : "Don't be rude ! I'm the oldest, you are wrong. Now look at the target.",
		"next" : "lvl_3_aim_4"
	},
	
	"lvl_3_aim_4": {
		"texture" : "imp",
		"anim": "aiming",
		"wait" : true,
		"text" : "How many members do you have... Well I don't care. You have as many tries to shot this and get to the portal, if you fail hit the retry button and go f*ck yourself noob.",
		"next" : "lvl_3_aim_5"
	},
	
	"lvl_3_aim_5": {
		"texture" : "squeleton",
		"text" : "But, it's miles away !! Oh god !",
		"next" : "lvl_3_aim_6"
	},
	
	"lvl_3_aim_6": {
		"texture" : "unknown",
		"text" : "Don't swear in my house please, that's not polite, like [b]at all[/b].",
	},
	
	"lvl_3_joke_vertigo": {
		"texture" : "imp",
		"text" : "I must confess something...",
		"next" : "lvl_3_joke_vertigo_2"
	},
	
	"lvl_3_joke_vertigo_2": {
		"texture" : "imp",
		"text" : "I am afraid of heights and that huge empty space scares the sh*t out of me, don't fall mate !",
	},
	
	"lvl_3_joke_jump": {
		"texture" : "imp",
		"text" : "\"Oh Nooo, not that again !\" Well screw you, this is what we call a hard training, even if we imps can fly. Now, OBEY AND GO AHEAD SQUELETON !",
	},
	
	"lvl_3_jump": {
		"texture" : "squeleton",
		"text" : "Well, where should I go now ?... Why are you laughing ?",
		"next" : "lvl_3_jump_2"
	},

	"lvl_3_jump_2": {
		"texture" : "imp",
		"text" : "Isn't that obvious ? We went left, then right. Now we're over our start point and I can stop laughting because you just went ahead without any second thought !",
		"next" : "lvl_3_jump_3"
	},

	"lvl_3_jump_3": {
		"texture" : "squeleton",
		"text" : "I did all these jumps for NOTHING ?!",
		"next" : "lvl_3_jump_4"
	},
	
	"lvl_3_jump_4": {
		"texture" : "Imp",
		"text" : "Yes haha !",
		"next" : "lvl_3_jump_5"
	},
	
	"lvl_3_jump_5": {
		"texture" : "squeleton",
		"text" : "Eeeeh... Let's jump and go right then...",
	},
	
	
	"lvl_4_intro": {
		"texture" : "imp",
		"text" : "Hum... Huuum... Well.",
		"next" : "lvl_4_intro_2"
	},
	
	"lvl_4_intro_2": {
		"texture" : "squeleton",
		"text" : "What's the matter ?",
		"next" : "lvl_4_intro_3"
	},
	
	"lvl_4_intro_3": {
		"texture" : "imp",
		"text" : "Nothing's wrong.",
		"next" : "lvl_4_intro_4"
	},
	
	"lvl_4_intro_4": {
		"texture" : "squeleton",
		"text" : "No laugh, no joke, there is definitly something !",
		"next" : "lvl_4_intro_5"
	},
	
	"lvl_4_intro_5": {
		"texture" : "imp",
		"text" : "You're right, I can't tell where we are.",
		"next" : "lvl_4_intro_6"
	},
	
	"lvl_4_intro_6": {
		"texture" : "squeleton",
		"text" : "Are we LOST ? For real ?!",
		"next" : "lvl_4_intro_7"
	},
	
	"lvl_4_intro_7": {
		"texture" : "imp",
		"text" : "No !.... Yes. Well this place is huge you kno-",
		"next" : "lvl_4_intro_8"
	},
	
	"lvl_4_intro_8": {
		"texture" : "squeleton",
		"text" : "But you live here for hundred of years now !",
		"next" : "lvl_4_intro_9"
	},
	
	"lvl_4_intro_9": {
		"texture" : "imp",
		"text" : "Well, I just became an imp... Like two days ago... Anyway, my map says it's right.",
		"next" : "lvl_4_intro_10"
	},
	
	"lvl_4_intro_10": {
		"texture" : "squeleton",
		"text" : "(looking the empty place left) Well. Genius.",
		"next" : "lvl_4_intro_11"
	},
	
	"lvl_4_intro_11": {
		"texture" : "imp",
		"text" : "Heee, don't f*ck with me boy. There is teleporter just there.",
	},
	
	"lvl_4_big_jump": {
		"texture" : "squeleton",
		"text" : "Ho, that's a big pit here.",
		"next" : "lvl_4_big_jump_2"
	},
	
	"lvl_4_big_jump_2": {
		"texture" : "imp",
		"text" : "Be a real sniper and stack two bones on the same point, like a bone bridge.",
		"next" : "lvl_4_big_jump_3"
	},
	
	"lvl_4_big_jump_3": {
		"texture" : "squeleton",
		"text" : "I can't tell if that bridge will support my weight, but that deserves a try.",
	},
	
	"lvl_4_big_jump_bis": {
		"texture" : "squeleton",
		"text" : "Oh you must be kidding, you better be kidding, ARE YOU F*CKING KIDDING ME ?!",
		"next" : "lvl_4_big_jump_bis_2"
	},
	
	"lvl_4_big_jump_bis_2": {
		"texture" : "imp",
		"text" : "(Did he just say it ?) Calm do-",
		"next" : "lvl_4_big_jump_bis_3"
	},
	
	"lvl_4_big_jump_bis_3": {
		"texture" : "squeleton",
		"text" : "Oh shut up b*tch, you can't even read a map dumb ass, f*ck you !",
		"next" : "lvl_4_big_jump_bis_4"
	},
	
		
	"lvl_4_big_jump_bis_4": {
		"texture" : "imp",
		"text" : "...",
	},
	
	"lvl_final_intro" : {
		"texture" : "devil",
		"anim" : "intro_1",
		"wait" : true,
		"text" : "Welcome my dear.",
		"next" : "lvl_final_intro_2"
	},
	
	"lvl_final_intro_2" : {
		"texture" : "squeleton",
		"text" : "What the.. Why am I here ? ... Are you the Devil ?",
		"next" : "lvl_final_intro_3"
	},
	
	"lvl_final_intro_3" : {
		"texture" : "devil",
		"text" : "Indeed, now, tell me your story.",
		"next" : "lvl_final_intro_4"
	},
	
	"lvl_final_intro_4" : {
		"texture" : "squeleton",
		"text" : "My... My story ?",
		"next" : "lvl_final_intro_5"
	},
	
	"lvl_final_intro_5" : {
		"texture" : "squeleton",
		"text" : "You, want to know my storry. Why I am here ? Let me explain.",
		"next" : "lvl_final_intro_6"
	},
	
	"lvl_final_intro_6" : {
		"texture" : "squeleton",
		"text" : "I spent my whole life waiting for the next day, no motivation at all. Call it Sloth if you want it.",
		"next" : "lvl_final_intro_7"
	},
	
	"lvl_final_intro_7" : {
		"texture" : "squeleton",
		"text" : "In the morning, I always eat.. I ate way too much, Call it Gluttony if you want.",
		"next" : "lvl_final_intro_8"
	},
	
	"lvl_final_intro_8" : {
		"texture" : "squeleton",
		"text" : "Going to work, I always see my really hot neighborand desire her, call it Lust if you want.",
		"next" : "lvl_final_intro_9"
	},
	
	"lvl_final_intro_9" : {
		"texture" : "squeleton",
		"text" : "Once at work, I claim being the best because I really am; call it Pride if you want.",
		"next" : "lvl_final_intro_10"
	},
	
	"lvl_final_intro_10" : {
		"texture" : "squeleton",
		"text" : "Even being the best, my co-workers all got promoted whereas I occupy the same job for many years, call it Envy if you want.",
		"next" : "lvl_final_intro_11"
	},
	
	"lvl_final_intro_11" : {
		"texture" : "squeleton",
		"text" : "So when I come back home, I play video game and rage on noobs, call it Anger if you want.",
		"next" : "lvl_final_intro_12"
	},
	
	"lvl_final_intro_12" : {
		"texture" : "squeleton",
		"text" : "So each and every day of my poor and lonely life, I commited the seven deadly sins regarding the holy sh*tty book.",
		"next" : "lvl_final_intro_13"
	},
	
	"lvl_final_intro_13" : {
		"texture" : "squeleton",
		"text" : "I died crashed by a f*cking car. I used to be a good guy you know. I used to be calm, only raging on games.",
		"next" : "lvl_final_intro_14"
	},
	
	"lvl_final_intro_14" : {
		"texture" : "squeleton",
		"text" : "And once all of this crap finally ended, hit by the devil mecanics, I get myself in this sh*tty world with a f*cker as a guide that can't f*cking read a map without being an assh*le !",
		"next" : "lvl_final_intro_15"
	},
	
	"lvl_final_intro_15" : {
		"texture" : "squeleton",
		"text" : "When I arrived, I instinctly tried to get back to life, begging this dumb ass for help, but now I understand...",
		"next" : "lvl_final_intro_16"
	},
	
	"lvl_final_intro_16" : {
		"texture" : "squeleton",
		"text" : "THE WHOLE F*CKING UNIVERSE IS A BIG SMELLY SH*T and even dead you can't rest so F*CK ALL THAT SH*T, F*CKING F*CK ! ",
		"next" : "lvl_final_intro_17"
	},
	"lvl_final_intro_17" : {
		"texture" : "squeleton",
		"text" : "Only one hour with your assh*le of servant and I already want to destroy everything so HERE I AM F*CKER, come to me, it can't be worse !",
		"next" : "lvl_final_intro_18"
	},
	
	"lvl_final_intro_18" : {
		"texture" : "devil",
		"text" : "...",
		"next" : "lvl_final_intro_19"
	},
	
	"lvl_final_intro_19" : {
		"texture" : "devil",
		"text" : "Well, I saw you shy and concerned about the imp's rudeness, and look at that; you're a big boy now aren't you ?",
		"next" : "lvl_final_intro_20"
	},
	
	"lvl_final_intro_20" : {
		"texture" : "devil",
		"text" : "Let's conclude a deal, if you succeed beating me, I let you my throne. If you can't beat me, I will personnaly torture you, regenerating every dying cell to kill it again; I need distraction and you need to learn who's the boss.",
		"next" : "lvl_final_intro_21"
	},
	
	"lvl_final_intro_21" : {
		"texture" : "squeleton",
		"text" : "DIE F*CKER !",
		"next" : "lvl_final_intro_22"
	},
	
	"lvl_final_intro_22" : {
		"texture" : "unknown",
		"text" : "Big Badass Bag of Bone, shot the Devil's orb and you will succeed. Take care, it manipulates space and time.",
		"next" : "lvl_final_intro_23"
	},
	
	"lvl_final_intro_23" : {
		"texture" : "unknown",
		"anim": "intro_2",
		"wait" : true,
		"text" : "Good luck BBBB!",
	},
	
	"end" : {
		"texture" : "unknown",
		"anim" : "enter",
		"text" : "...",
		"next" : "end_2"
	},
	
	"end_2" : {
		"texture" : "unknown",
		"text" : "I pushed him very far this time, how rude can he be exactly haha. ... That was a fun one.",
		"next" : "end_3"
	},
	
	"end_3" : {
		"texture" : "unknown",
		"text" : "That was very fun. I remember when his body was just starting to rot, the arm thing was hilarious. Anyways, this time, I will play more stupid again, see how far I can go.",
		"next" : "end_4"
	},
	
	"end_4" : {
		"texture" : "unknown",
		"text" : "Oh, and I'm quite hungry, we will pass through the canteen instead of shoting and jumping area.",
		"next" : "end_5"
	},
	
	"end_5" : {
		"texture" : "unknown",
		"text" : "Also, I need new places, maybe this time I will really bring Cerberus haha. Anyway, let's go.",
		"next" : "end_6"
	},
	
	"end_6" : {
		"texture" : "unknown",
		"anim" : "reveal",
		"text" : "Welcome to your new reality dumb ass.",
		"next" : "end_7"
	},
	
	"end_7" : {
		"texture" : "squeleton",
		"text" : "Who's.. Who's talking ?",
		"next" : "end_8"
	},
	
	"end_8" : {
		"texture" : "imp",
		"text" : "Open your eyes! Ooooh, sorry, you don't have eyes anymore. Hahaha",
		"next" : "end_9"
	},
	
	"end_9" : {
		"texture" : "imp",
		"wait" : true,
		"text" : "(Hey, you, watching all of this... I hope you enjoy the journey. Thanks.)",
	},
}
	