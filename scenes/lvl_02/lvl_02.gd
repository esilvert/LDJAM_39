extends Node2D

# Triggers
onready var triggers = [
		]

const dialog_ids = [
	"lvl_2_intro",
	]

var current_dialog_idx = 0

# Ready
func _ready():
	g_globals.current_scene_behavior = self
	respawn()
	
	# Subscribe
	for trigger in triggers:
		trigger.connect("body_enter", self, "_on_body_enter")
	
	display_next()
	
# Exit
func _exit_tree():
	g_globals.current_scene_behavior = null

# Trigger a chat
func _on_body_enter(p_other):
	if p_other.is_in_group("player"):
		triggers[clamp(current_dialog_idx - 1, 0, triggers.size())].queue_free()
		display_next()
		
		
func display_next():
	if g_globals.diaglog_once.has(dialog_ids[current_dialog_idx]) == false:
		g_globals.diaglog_once.append(dialog_ids[current_dialog_idx])
		g_player.player.controls_enabled = false
		g_player.player.fsm.changeStateTo("idle")
		g_globals.chatbox.set_dialog(dialog_ids[current_dialog_idx])
		yield(g_globals.chatbox, "chat_ended")
		g_player.player.controls_enabled = true
	else:
		g_globals.chatbox.muted_chat(dialog_ids[current_dialog_idx])
	current_dialog_idx += 1
			

func respawn():
	g_player.reset()
	g_player.player.set_global_pos( get_node("spawn_1").get_global_pos() )