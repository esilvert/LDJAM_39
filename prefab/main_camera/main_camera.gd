extends Camera2D

#
# Main Camera
#

# Margin
export var MARGIN_DISTANCE = 32*4
export var CAMERA_DELAY = 0.2
export var ENABLED = true

# Accum for lerp
var accum = 0

# Ready
func _ready():
	set_fixed_process(true)
	
# Fixed process
func _fixed_process(delta):
	if ENABLED == true:
		var focus_pos 
		
		if g_globals.camera_focus == null :
			focus_pos = g_player.player.get_global_pos()
		else:
			focus_pos = g_globals.camera_focus
		
		var position = get_global_pos()
		var distance = position.distance_to(focus_pos)
		
		if g_player.player.is_dead == false :
			accum += delta
			if distance > MARGIN_DISTANCE:
#				position.x = lerp(position.x, focus_pos.x, clamp(accum / CAMERA_DELAY, 0, 1))
#				position.y = lerp(position.y, focus_pos.y, clamp(accum / CAMERA_DELAY, 0, 1))
				
				set_global_pos(focus_pos)
			else:
				accum = 0
		else:
			var viewport_rect = get_viewport_rect()
			viewport_rect.pos = position
			if viewport_rect.has_point(focus_pos) == false:
	#			g_globals.current_scene_behavior.respawn()
				g_scene_manager.reload_current_scene()
		