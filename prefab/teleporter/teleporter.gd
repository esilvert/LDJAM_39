extends Area2D

# Destination scene
export var DESTINATION_SCENE = "lvl_02"

# Ready
func _ready():
	connect("body_enter", self, "_on_body_enter")

# Callback
func _on_body_enter(p_body):
	if p_body.is_in_group("player"):
		g_sound_manager.play("portal")
		g_scene_manager.go_to_scene_path(DESTINATION_SCENE)
		