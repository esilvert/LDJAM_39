extends KinematicBody2D

#
# Bone
#

signal impact

# Velocity
var velocity = Vector2()

# Shape
onready var shape = g_node_utils.get_node(self, "shape")

var accum = 0

# Ready
func _ready():
	set_fixed_process(true)
	add_collision_exception_with(g_player.player)

func _exit_tree():
	g_globals.camera_focus = null

# Fixed process
func _fixed_process(delta):
	accum += delta
	velocity.y += Globals.GRAVITY * delta
	
	var rest = move(velocity * delta)
	if is_colliding():
		# Callback
		var collider = get_collider()
		_on_body_enter(collider)
		
		if  get_global_pos().distance_to(g_player.player.get_global_pos()) > 16:
				remove_collision_exception_with(g_player.player)
				set_fixed_process(false)
				emit_signal("impact")
				g_globals.camera_focus = null
				return
		
	g_globals.camera_focus = get_global_pos()
	
	# timeout
	if accum >= 3:
		set_fixed_process(false)
		g_globals.camera_focus = null
		emit_signal("impact")
		
	
# Callback body enter
func _on_body_enter(p_other):
	if p_other.is_in_group("bone_sensitive"):
		g_sound_manager.play("bone_impact")
		p_other._on_bone_impact(self)
		emit_signal("impact")
		g_globals.camera_focus = null
		set_fixed_process(false)