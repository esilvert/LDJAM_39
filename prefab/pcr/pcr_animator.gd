extends AnimationPlayer

#
# PCR animator
#


# Current anim
var current_anim = null

# Safe play
func play(p_id):
	if current_anim != p_id:
		current_anim = p_id
		.play(p_id)