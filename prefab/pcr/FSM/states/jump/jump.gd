extends "res://addons/net.kivano.fsm/content/FSMState.gd";
################################### R E A D M E ##################################
# For more informations check script attached to FSM node
#
#

export var JUMP_SPEED = 64

##################################################################################
#####  Variables (Constants, Export Variables, Node Vars, Normal variables)  #####
######################### var myvar setget myvar_set,myvar_get ###################
# Jump Impulse
export var JUMP_IMPULSE = 64
export var JUMP_ACCELERATION = 450

var total_impulse_given = 0
var total_gravity = 0

##################################################################################
#########                       Getters and Setters                      #########
##################################################################################
#you will want to use those
func getFSM(): return fsm; #defined in parent class
func getLogicRoot(): return logicRoot; #defined in parent class 

##################################################################################
#########                 Implement those below ancestor                 #########
##################################################################################
#you can transmit parameters if fsm is initialized manually
func stateInit(inParam1=null,inParam2=null,inParam3=null,inParam4=null, inParam5=null): 
	pass

#when entering state, usually you will want to reset internal state here somehow
func enter(fromStateID=null, fromTransitionID=null, inArg0=null,inArg1=null, inArg2=null):
#	logicRoot.velocity.y -= JUMP_IMPULSE
	total_impulse_given = 0
	total_gravity = 0
	logicRoot.gravity_enabled = true

# when updating state, paramx can be used only if updating fsm manually
func update(deltaTime, param0=null, param1=null, param2=null, param3=null, param4=null):
	# Jump
	if Input.is_action_pressed("pcr_jump") and logicRoot.controls_enabled == true:
		if abs(total_impulse_given) < JUMP_IMPULSE: 
			total_impulse_given -= JUMP_ACCELERATION * deltaTime
			logicRoot.velocity.y -= JUMP_ACCELERATION * deltaTime
		else: total_impulse_given = -JUMP_IMPULSE
	
	# Update velocity
	logicRoot.velocity.x = JUMP_SPEED * logicRoot.direction_vector.x
	
	# Anim
	logicRoot.animator.play("jump_" + logicRoot.get_current_member_state_str() )
#

#when exiting state
func exit(toState=null):
	logicRoot.velocity.y = 0
	g_sound_manager.play("land", true)
	pass

##################################################################################
#########                       Connected Signals                        #########
##################################################################################

##################################################################################
#########     Methods fired because of events (usually via Groups interface)  ####
##################################################################################

##################################################################################
#########                         Public Methods                         #########
##################################################################################

##################################################################################
#########                         Inner Methods                          #########
##################################################################################

##################################################################################
#########                         Inner Classes                          #########
##################################################################################
