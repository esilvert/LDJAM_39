extends "res://addons/net.kivano.fsm/content/FSMState.gd";
################################### R E A D M E ##################################
# For more informations check script attached to FSM node
#
#

##################################################################################
#####  Variables (Constants, Export Variables, Node Vars, Normal variables)  #####
######################### var myvar setget myvar_set,myvar_get ###################
# Counter
var accum = 0

# Power clamp
export var MAX_POWER = 1500
export var MIN_POWER = 100

# Power bar
onready var power_bar = g_globals.gui_power_bar


# Power per second
const POWER_PER_SECOND = 500
##################################################################################
#########                       Getters and Setters                      #########
##################################################################################
#you will want to use those
func getFSM(): return fsm; #defined in parent class
func getLogicRoot(): return logicRoot; #defined in parent class 

##################################################################################
#########                 Implement those below ancestor                 #########
##################################################################################
#you can transmit parameters if fsm is initialized manually
func stateInit(inParam1=null,inParam2=null,inParam3=null,inParam4=null, inParam5=null): 
	pass

#when entering state, usually you will want to reset internal state here somehow
func enter(fromStateID=null, fromTransitionID=null, inArg0=null,inArg1=null, inArg2=null):
	accum = 0
	power_bar.set_max(MAX_POWER)
	power_bar.set_min(MIN_POWER)
	pass

#when updating state, paramx can be used only if updating fsm manually
func update(deltaTime, param0=null, param1=null, param2=null, param3=null, param4=null):
	logicRoot.velocity *= 0.85
	accum += deltaTime
	power_bar.set_val( clamp(accum * POWER_PER_SECOND, MIN_POWER, MAX_POWER) )
	
	# Update direction
	var mouse_pos = g_player.player.get_global_mouse_pos()
	var angle = g_player.player.get_angle_to(mouse_pos) - PI * 0.5
	angle *= -1
	if abs(rad2deg(angle)) > 90:
		logicRoot.direction = logicRoot.EDIRECTION.LEFT
	else:
		logicRoot.direction = logicRoot.EDIRECTION.RIGHT
#when exiting state
func exit(toState=null):
	logicRoot.throw_power = clamp(accum * POWER_PER_SECOND, MIN_POWER, MAX_POWER)
	pass

##################################################################################
#########                       Connected Signals                        #########
##################################################################################

##################################################################################
#########     Methods fired because of events (usually via Groups interface)  ####
##################################################################################

##################################################################################
#########                         Public Methods                         #########
##################################################################################

##################################################################################
#########                         Inner Methods                          #########
##################################################################################

##################################################################################
#########                         Inner Classes                          #########
##################################################################################
