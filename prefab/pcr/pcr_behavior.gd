extends KinematicBody2D

#
# Pcr Behavior
#

# The impulse given to the bone
export(Vector2) var THROW_IMPULSE

# The offset
export(Vector2) var BONE_OFFSET

# Bone scene
var bone_scene = preload("res://prefab/bone/bone.tscn")

# Sprite
onready var sprite = g_node_utils.get_node(self, "sprite")

# Animator
onready var animator = g_node_utils.get_node(self, "animator")

# FSM
onready var fsm = g_node_utils.get_node(self, "fsm")

# trigger area
const TRIGGER_AREA_RIGHT_OFFSET = Vector2(2,6)
const TRIGGER_AREA_LEFT_OFFSET = Vector2(-8,6)
onready var trigger_area = g_node_utils.get_node(self, "trigger_area")

# Direction management
enum EDIRECTION {NONE, LEFT, RIGHT, UP, DOWN}
const DIRECTION_TO_VELOCITY = [
	Vector2(0,  0 ), 	# NONE
	Vector2(-1, 0 ), 	# LEFT
	Vector2(1,  0 ), 	# RIGHTs
	Vector2(0,  -1), 	# UP
	Vector2(0,  1 ) 	# DOWN
]

var direction = EDIRECTION.NONE setget set_direction
var direction_vector = Vector2()

func set_direction(p_value):
	direction = p_value 
	sprite.set_flip_h(direction == EDIRECTION.LEFT)
	direction_vector = DIRECTION_TO_VELOCITY[direction]
	if direction == EDIRECTION.LEFT:
		trigger_area.set_pos(TRIGGER_AREA_LEFT_OFFSET)
	else:
		trigger_area.set_pos(TRIGGER_AREA_RIGHT_OFFSET)

# Member Count
enum EMEMBER_STATE {FULL, ONE_ARM, TWO_LEGS, ONE_LEG, SKULL}

var current_member_state = EMEMBER_STATE.FULL

func get_current_member_state_str():
	return state_to_str(current_member_state)

# Velocity
var velocity = Vector2()

# Jump
var can_jump = true
var is_jumping = false
var gravity_enabled = true

# Throw
var throw_power = 50

# Controls
var controls_enabled = true

func state_to_str(p_state):
	if p_state == EMEMBER_STATE.FULL:
		return "full"
	elif p_state == EMEMBER_STATE.ONE_ARM:
		return "one_arm"
	elif p_state == EMEMBER_STATE.TWO_LEGS:
		return "two_legs"
	elif p_state == EMEMBER_STATE.ONE_LEG:
		return "one_leg"
	else:
		return "skull"
#

# Colliding
var is_touching_ground = false
var is_touching_wall = false
var collision_normal = Vector2(0,0)

# Dead
var is_dead = false

# Ready
func _ready():
	g_sound_manager.play("spawn")
	fsm.connect("stateChanged", self, "_on_state_changed")
	is_dead = false
	set_fixed_process(true)

# Enter
func _enter_tree():
	g_player.player = self

# Exit
func _exit_tree():
	g_player.player = null

# Fixed Process
func _fixed_process(delta):
	# Update direction
	update_direction()
	
	# Gravity
	if gravity_enabled == true:
		velocity.y += Globals.GRAVITY * delta
		if abs(velocity.y) >= Globals.GRAVITY * 1.5: # 2 seconds falling
			is_dead = true
	
	# Movement
	move(velocity * delta)
	
	# Trigger
	if Input.is_action_pressed("pcr_trigger") == true and controls_enabled == true:
		var bodies = trigger_area.get_overlapping_bodies()
		for body in bodies:
			if body.is_in_group("bone"):
				body.queue_free()
				current_member_state -= 1
			if body.has_method("on_player_trigger"):
				body.on_player_trigger(self)
	

# Callback from FSM
func _on_state_changed(p_new_state, p_previous_state):
	g_debug.print_message("PCR switched from state " + p_previous_state + " to state " + p_new_state + ".") 
	
# Forbidden set
func set_forbidden(p_value):
	g_debug.assert_message(false, "pcr_behavior.gd.var=(): Velocity is private.")

# Move and slide
func move(p_movement):
	# X movement
#	if test_move(Vector2(p_movement.x, 0)) == false:
	var rest = .move(Vector2(p_movement.x, 0))
	
	if is_colliding() == true:
		revert_motion()
		collision_normal = get_collision_normal()
		if abs(collision_normal.x) != 1:
			is_touching_wall = false
			rest = collision_normal.slide(rest)
			.move(rest)
		else:
			is_touching_wall = true
#	else :
#		is_touching_wall = true
	
	# Y movement
	.move( Vector2(0, p_movement.y))
	var is_colliding = is_colliding()
	
	if is_colliding():
		collision_normal.y = get_collision_normal().y
	#	
		# On ground means no Y velocity to the ground
		if collision_normal.y < 0:
			velocity.y = 0
			is_touching_ground = true
		else:
			is_touching_ground = false
	else  :
		is_touching_ground = false

	
# Update the direction (and all)
func update_direction():
	var left = Input.is_action_pressed("pcr_left") and controls_enabled == true
	var right = Input.is_action_pressed("pcr_right") and controls_enabled == true
	
	if left != right:
		if left == true:
			set_direction(EDIRECTION.LEFT)
		elif right == true:
			set_direction(EDIRECTION.RIGHT)
	#
#

# Throw
func throw():
	g_debug.print_message("pcr.throw(): Throw has been called from member: " + str(current_member_state))
	var bone_instance = bone_scene.instance()
	var offset  = BONE_OFFSET
	if direction == EDIRECTION.LEFT:
		offset.x *= -1 
	
	var mouse_pos = g_player.player.get_global_mouse_pos()
	var angle = g_player.player.get_angle_to(mouse_pos + offset) - PI * 0.5
	angle *= -1
	
	bone_instance.velocity = Vector2(cos(angle), sin(angle)) * throw_power
	bone_instance.set_global_pos(get_global_pos() + offset)
	get_parent().add_child(bone_instance)
	controls_enabled = false
	yield(bone_instance, "impact")
	controls_enabled = true
	
func play_step():
	g_sound_manager.play("step")