extends ProgressBar

export(Vector2) var OFFSET = Vector2(-15, -30)

# Ready
func _ready():
	set_hidden(true)
	g_globals.gui_power_bar = self
	set_fixed_process(true)

# Fixed process
func _fixed_process(delta):
	if g_player.player and g_player.player.fsm.getStateID() == "charge":
		set_hidden(false)
		set_global_pos( g_player.player.get_global_pos() + OFFSET )
	else: 
		set_hidden(true)
#		pass