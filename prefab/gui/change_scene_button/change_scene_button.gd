extends "res://prefab/gui/tr_button/tr_button.gd"

# Destination Scene
export var destination_scene = "splashscreen"

# Pressed
func _pressed():
	g_scene_manager.go_to_scene_path(destination_scene)