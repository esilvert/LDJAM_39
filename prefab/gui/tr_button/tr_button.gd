extends Button

# Translation Key
export var TR_KEY = "ACTION"
export var TR_TOOLTIP = "TOOLTIP"

# Ready
func _ready():
	# Set translated text
	set_text( tr(TR_KEY) )
	set_tooltip( tr(TR_TOOLTIP) )