extends StaticBody2D

# The shape
onready var shape = g_node_utils.get_node(self, "shape")
onready var sprite =g_node_utils.get_node(self, "sprite")
var triggered = false

func _ready():
	sprite.set_frame(0)
	triggered = false

# Callback from the bone
func _on_bone_impact(p_bone):
	if triggered == false:
		g_sound_manager.play("lock")
		triggered = true
		get_tree().call_group(0, "doors", "_on_bone_target_hit", self, p_bone)
		sprite.set_frame(1)